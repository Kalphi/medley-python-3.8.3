def addition():
    result: int = 10 + 2
    return result


def addition2():
    return 10 + 12


def addition3():
    return 10 + 9


def multiply():
    return 5 * 6


def get_message():
    return "Le résultat des 2 opérations est : "


def addition(n):
    return 10 + n


def addition(n=45):
    return 10 + n


print("Le résultat du calcul est : ", addition())
# print("Le résultat du calcul est : ", addition2())
# print("Le résultat du calcul est : ", addition3())
# print("Le résultat du calcul est : ", multiply())
# print(get_message(), addition(), multiply())
print("Le résultat de l'addition est : ", addition(12))


# créer une fonction max() qui va renvoyer le reultat le plus haut parmis 2 valeurs
def max(a, b):
    if a > b:
        return a
    else:
        return b

# notion de récurcivité
def add(a):
    a += 1
    print(a)
    if a < 10:
        add(a)


first_value = int(input("Entrez la première valeur (a) : "))
second_value: int = int(input("Entrez la seconde valeur (b) : "))
max_value = max(first_value, second_value)
print("La valeur la plus grande est : ", max(first_value, second_value))
add(2)