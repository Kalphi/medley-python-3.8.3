from tkinter import *
import webbrowser


class MyApp:

    def __init__(self) -> object:
        self.window = Tk()
        self.window.title("My Application")
        self.window.geometry("720x480")
        self.window.minsize(480, 360)
        self.window.iconbitmap("avatar_dragon.ico")
        self.window.config(background='grey')

        # initialization des composants
        self.frame = Frame(self.window, bg='grey')

        # creation des composants
        self.create_widgets()

        # empaquetage
        self.frame.pack(expand=YES)

    def create_widgets(self):
        self.create_title()
        self.create_subtitle()
        self.create_cv_button()

    def create_title(self):
        label_title = Label(self.frame, text="Bienvenue sur l'application", font=("Courrier", 40), bg='grey', fg='white')
        label_title.pack()

    def create_subtitle(self):
        label_subtitle = Label(self.frame, text="Hey it's me", font=("Courrier", 25), bg='grey', fg='white')
        label_subtitle.pack()

    def create_cv_button(self):
        cv_button = Button(self.frame, text="Ouvrir mon CV", font=("Courrier", 25), bg='white', fg='grey', command=self.open_cv_ren)
        cv_button.pack(pady=25, fill=X)

    def open_cv_ren(self):
        webbrowser.open_new("https://rvitali.labo-ve.fr/")


# afficher
app = MyApp()
app.window.mainloop()
