from tkinter import *
import webbrowser
from tkinter import Tk, Radiobutton


def open_cv_ren():
    webbrowser.open_new("https://rvitali.labo-ve.fr/")


# créer une première fenêtre
window: Tk = Tk()

# personnaliser la fenêtre
window.title("My Application")
window.geometry("720x480")
window.minsize(480, 360)
window.iconbitmap("avatar_dragon.ico")
window.config(background='grey')

# créer un curseur
scale_w = Scale(window)

# afficher le curseur
scale_w.pack(expand=YES)

# créer un Radio button
spin_w: Radiobutton = Radiobutton(window, value=0)
spin_w.pack(expand=YES)

# créer la frame
frame = Frame(window, bg='grey', bd=1, relief=SUNKEN)

# ajouter un premier texte
label_title = Label(frame, text="WELCOME to my Apply", font=("Arial", 30), bg='grey', fg='white')
# label_title.pack(side=TOP)
label_title.pack(expand=YES)

# ajouter un second texte
label_subtitle = Label(window, text="Hey it's me", font=("Courrier", 20), bg='grey', fg='white')
label_subtitle.pack(expand=YES)

# ajouter un premier bouton
cv_button = Button(frame, text="Lien vers mon CV", font=("Courrier", 10), bg='white', fg='grey', command=open_cv_ren)
cv_button.pack(ipady=25, fill=X)

# ajouter la frame
frame.pack(expand=YES)

# afficher
window.mainloop()
