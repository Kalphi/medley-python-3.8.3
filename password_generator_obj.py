from random import randint, choice
from tkinter import *
import string
from tkinter import PhotoImage, Entry, Tk, Button

from password_generator import generate_password


class PasswordGenerator:
    generate_password_button: Button
    password: str
    password_entry: Entry
    image: PhotoImage
    canvas: Canvas
    menu_bar: Menu
    file_menu: Menu
    right_frame: Frame

    def __init__(self):
        self.window = Tk()
        self.generate_password = None
        self.frame = Frame(self.window, bg='grey')
        self.height = 300
        self.generate_password_button = Button(self.right_frame, text="Générer", font=("Helvetica", 20), bg='grey', fg='white', command=generate_password)
        self.label_title: Label = Label(self.right_frame, text="Mot de passse", font=("Helvetica", 20), bg='grey', fg='white')
        self.width = 300
        self.canvas = Canvas(self.frame, width=self.width, height=self.height, bg='grey', bd=0, highlightthickness=0)
        self.password_min = 16
        self.password_max = 64
        self.all_chars = string.ascii_letters + string.punctuation + string.digits
        self.password = "".join(choice(self.all_chars) for self.password in range(randint(self.password_min, self.password_max)))
        self.password_entry.delete(0, END)
        self.password_entry.insert(0, self.password)

    # créer la fenêtre
    def create_window(self):
        self.window.title("Générateur de mot de passe")
        self.window.geometry("720x480")
        self.window.iconbitmap("avatar_dragon.ico")
        self.window.config(background='grey')
        self.image = PhotoImage(file="shield.png").zoom(35).subsample(32)

    # création de la frame principale
    def create_primary_frame(self):
        pass

    # création d'image
    def create_image(self):
        self.canvas.create_image(self.width / 2, self.height / 2, image=self.image)
        self.canvas.grid(row=0, column=0, sticky=W)

    # création d'une sous boite
    def create_right_frame(self):
        self.right_frame = Frame(self.frame, bg='grey')

    # créer un titre
    def create_title(self):
        self.label_title.pack()

    # créer un champs/entrée/input
    def create_field(self):
        self.password_entry = Entry(self.right_frame, font=("Helvetica", 20), bg='grey', fg='white')
        self.password_entry.pack()

    # créer un bouton
    def create_button(self):
        self.generate_password_button.pack(fill=X)

    # on place la sous boite à droite de la frame principale
    def right_frame_grid(self):
        self.right_frame.grid(row=0, column=1, sticky=W)

        # afficher la frame principale
        self.frame.pack(expand=YES)

    # création d'une barre de menu
    def menu_bar(self):
        self.menu_bar = Menu(self.window)

    # créer un premier menu
    def file_menu(self):
        self.file_menu = Menu(self.menu_bar, tearoff=0)
        self.file_menu.add_command(label="Nouveau", command=self.generate_password)
        self.file_menu.add_command(label="Quitter", command=self.window.quit)
        self.menu_bar.add_cascade(label="Fichier", menu=self.file_menu)

    # configurer notre fenêtre pour ajouter cette menu bar
    def add_menu_bar(self):
        self.window.config(menu=self.menu_bar)


# afficher la fenêtre
app = PasswordGenerator()
app.window.mainloop()
