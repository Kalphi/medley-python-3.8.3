import statistics

from statistics import mean
from random import shuffle

# créer une liste qui va stocker des pseudos pour simuler un jeu en ligne
# Ren, Emy, Titou, ...
from typing import List

online_players: List[str] = ["Ren", "Emy", "Titou", "Dédé"]
print(online_players)
print(online_players[2])
print(online_players[len(online_players) - 1])

# modifier 'Ren' -> 'Renaud'
online_players[0] = "Renaud"

print(online_players)
online_players.insert(2, "EmyFay")
print(online_players)
online_players[2:3] = ["Anne", "Marie"]
print(online_players)

# en ajouter d'autres
# on imagine qu'un joueur "Gamerspoiler" se connecte
online_players.append("Gamerspoiler")
print(online_players)
online_players.extend(["Maurice", "Marie-France"])
print(online_players)

# on imagine que le joueur "Emy" se déconnecte
del online_players[1]
print(online_players)
# ou que se soit le tour de "Marie" de se déconnecter
online_players.pop(2)
print(online_players)
# ou que "Dédé" se déconnecte
online_players.remove("Dédé")
print(online_players)

# on supprime tous les joueurs
del online_players[:]
# ou
online_players.clear()
print(online_players)

# exemple: Jouer à la maitresse

notes: List[int] = [8, 9, 14,
                    16, 15, 20,
                    ]
print(notes)
print(notes[2])
shuffle(notes)
print(notes)

# module : statistics
result = statistics.mean(notes)
# ou
result = mean(notes)
print("La moyenne des notes est de : {}".format(result))

text: List[str] = input("Entrez une chaine de la forme (email-pseudo-motdepasse)").split("-")
print(text)
print("Salut {}, ton email {}, ton mot de passe {}".format(text[1], text[0], text[2]))

# générateur de phrases
# demander en console une chaine de la forme "mot1/mot2/mot3/mot4..." est la transformer en liste

chained_words: List[str] = input("Veuillez saisir une phrase dont la forme est (mot1/mot2/mot3/mot4/...) :").split("/")
print(chained_words)

# la mélanger
shuffle(chained_words)
print(chained_words)
# variable pour la longueur de la liste
chained_words_length: int = len(chained_words)
# si le nombre d'éléments de cette liste est infèrieur à 10 -> afficher les deux premiers mots
if chained_words_length < 10:
    print(chained_words[0:2])
# sinon les trois derniers
else:
    print(chained_words[chained_words_length - 1], chained_words[chained_words_length - 2], chained_words[chained_words_length - 3])
