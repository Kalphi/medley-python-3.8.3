class Player:

    def __init__(self, pseudo, health, attack):
        self.pseudo = pseudo
        self.health = health
        self.attack = attack
        print("Bienvenue au joueur : ", pseudo, " / Points de vie : ", health, " / Points d'attaque : ", attack)

    def get_pseudo(self):
        return self.pseudo

    def get_health(self):
        return self.health

    def get_attack(self):
        return self.attack

    def damage(self, damage):
        self.health -= damage
        print("Aie... vous venez de subir", damage, "dégats !")

    def attack_player(self, target_player):
        target_player.damage(self.attack)


player1 = Player("Ren", 20, 3)
print("Pseudo : ", player1.get_pseudo())
player1.damage(3)
print("Vous possédez désormais ", player1.get_health(), " points de vie")
player2 = Player("Emy", 30, 5)
player1.attack_player(player2)
print(player1.get_pseudo(), "attaque", player2.get_pseudo())

print("Bienvenue au joueur : ", player1.get_pseudo, " / Points de vie : ", player1.get_health, " / Points d'attaque : ",
      player1.get_attack_value())
print("Bienvenue au joueur : ", player2.get_pseudo, " / Points de vie : ", player2.get_health, " / Points d'attaque : ",
      player2.get_attack_value())
