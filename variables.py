def main():
    # création d'une variable 'username' de type String ayant pour valeur ("Renaud")
    username = "Renaud"
    # créatin d'une variable 'age' de type Entier ayant pour valeur ("44")
    age = 44
    # création d'une variable 'wallet' de type Float ayant pour valeur ("599.7")
    wallet = 599.7
    # affiche le username et age
    print(username, age, wallet)
    username = "émilie"
    print(username)
    # change la valeur de la variable 'age' de (44) à (34)
    age = 34
    # affiche la nouvelle valeur
    print(age)
    # ajout de (1) à la valeur (34)
    age = age + 1
    # affiche la nouvelle valeur (35)
    print(age)
    # on multiplie la variable 'age' (35) par 'age' (35)
    age = age * age
    print(age)
    print("Salut {0} j\'ai {1} ans et je possède {2} €...".format(username, str(age), str(wallet)))
    # récolter une première note
    note1 = int(input("Entrez la première note :"))
    # seconde note
    note2 = int(input("Entrez la seconde note :"))
    # troisième note
    note3 = int(input("Entrez la troisième note :"))
    # calcul de la moyenne
    result = (note1 + note2 + note3) / 3
    # afficher le résultat
    print("La moyenne est de " + str(result))
    # récolter la valeur du porte monnaie
    wallet = 250
    # création du produit d'une valeur de 50
    productprice = 50
    # calcul de la somme après achat
    result = (wallet - productprice)
    # afficher la nouvelle valeur du porte monnaie après l'achat
    print("la somme restante est de :" + str(result))
    # création d'une variable 'is_happy' de type booléene ayant pour valeur (True)
    # is_happy = True
    # print("hello Renaud")
    # print("Hé bien le bonjour les amis !")


if __name__ == '__main__':
    main()
