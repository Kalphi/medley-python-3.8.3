

# code
wallet: int = 5000
computer_price = 800

# vérifier que le prix de l'ordinateur est infèrieur à 900€
if computer_price <= wallet:
    if computer_price > 1000: # on peu remplacer 'and' par 'or'
        print("L'achat est possible")
        wallet = wallet - computer_price
        # ou
        wallet -= computer_price
    else:
        print("L'achat n'est pas possible, vous n'avez que {}€".format(wallet))
else:
    print("L'achat n'est pas possible, vous n'avez que {}€".format(wallet))

# conditions ternaires
text =  ("L'achat est possible","L'achat est impossible")[computer_price <= 1000]
print(text)

print(wallet)

# exemple: Système de vérification de mot de passe
password: str = input("Entrez votre mot de passe : ")
password_length = len(password)

# vérifier si le mot de passe est infèrieur à 8 caractères
if password_length <= 8:
    print("Mot de passe trop court !!!")
elif 8 < password_length < 12:
    print("Mot de passe moyen !!!")
else:
    print('Mot de passe parfait !!!')
    print(password_length)

# Place de cinéma
# récolter l'âge de la personne "Quel est votre âge?"
age = int(input("Quel est votre âge ?: "))

# si la personne est mineure
if age < 18:
    total_price: int = 7
# si la personne est majeure
else:
    total_price = 12
# ou alors en ternaire
# total_price = (7, 12)[age < 18]

# souhaitez-vous du pop corn?
pop_corn_request = input("Souhaitez-vous du pop corn ? (Oui, Non)")

# si Oui
if pop_corn_request == "Oui":
    total_price += 5
# prix total à payer
print("Vous devez payer", total_price, "€")
