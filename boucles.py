from random import randint
from typing import Any, Union

print("Vous êtes le client n°1")

# for : pour une valeur de départ (1) jusqu'à une valeur d'arrivée (...)
for num_client in range(1, 5):
    print("Vous êtes le client n°: ", num_client)

# for each : pour chaque valeur d'une liste donnée
print("Email envoyé à : renvitali.rv@gmail.com")
print("Email envoyé à : frerot109@gmail.com")
print("Email envoyé à : renaud.vitali@labo-ve.fr")

# lister les Emails
emails = ['renvitali.rv@gmail.com', 'frerot109@gmail.com', 'renaud.vitali@labo-ve.fr', 'huberttoni@gmail.com',
          'renaud.vitali@orange.fr']

# blacklist
blacklist = ['renvitali.rv@gmail.com', 'renaud.vitali@orange.fr']

# pour chacune des valeurs, j'affiche "Email envoyé à [INSERER EMAIL]"
email: str
for email in emails:

    if email in blacklist:
        print("Email {} interdit ! Envoi impossible...", email)
        continue
        # ou
        break
    print("Email envoyé à : ", email)

# while : tant qu'une condition est vraie
# salarié 1500€ / mois
salary: int = 1500

# tant que salaire < 1200€, + 120€
while salary < 2000:
    # ajouter 120€ au salaire
    salary += 120
    # afficher le résultat
    print("Votre salaire actuel est de ", salary, "€")

print("Fin du programme")

# un Youtubeur "Kalphi", 2500 abonnés
subscribers_count: int = 2500

# il gagne 10% d'audience supplémentaire / mois
months = 0

# on souhaite estimer, combien aura t'il d'abonnés après 2 ans (24 mois)
while months <= 24:
    # augmenter l'audience
    # + 30% : 1 + (30/100) : 1.3
    # - 20% : 1 - (20/100) : 0.8
    subscribers_count *= 1.10

    # afficher le nombre d'abonnés
    print("Le nombre d'abonnés est de : {}", subscribers_count)

    # on passe au mois suivant
    months += 1

# TP : Jeu du Juste prix
# choisir un prix entre 1€ et 1000€
just_price: Union[int, Any] = randint(1, 1000)

# partie en cours
playing: bool = True
end_game: str = "La partie est terminée"
# tant que le jeu n'est pas fini
for try_number in range(1, 11):
    # demander au joueur d'entrer un prix dans la console
    player_price = int(input("Entrez un prix : "))

    # si le prix est juste
    if player_price == just_price:
        print("Gagné!!!")

    # si le prix du joueur est infèrieur au juste prix
    elif player_price < just_price:
        print("Votre prix est trop petit!")

    # si le prix du joueur est plus grand que le juste prix
    elif player_price > just_price:
        print("Votre prix est trop grand!")

        # fin du jeu après la boucle
        print(end_game)
        playing = False
