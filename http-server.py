# coding:utf-8
import http.server
from http.server import CGIHTTPRequestHandler, HTTPServer
from typing import Type

port = 80
address = ("", port)

server = http.server.HTTPServer

handler: Type[CGIHTTPRequestHandler] = http.server.CGIHTTPRequestHandler
handler.cgi_directories = ["/"]

httpd: HTTPServer = server(address, handler)

print(f"Serveur démarré sur le PORT {port}")
httpd.serve_forever()
